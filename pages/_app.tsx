import { Fragment, MutableRefObject, useRef, useEffect } from 'react'
import { useRouter } from 'next/router'
import { LocomotiveScrollProvider as RLSProvider, useLocomotiveScroll } from 'react-locomotive-scroll'
import gsap, { ScrollTrigger } from '../components/gsap'

import Layouts from 'layouts'

import '../styles/main.sass'
import '../styles/global.css'
import { SmootherProvider } from 'context/SmootherContext'
import { MenuProvider } from 'context/MenuContext'

const ScrollTriggerProxy = () => {
  const { scroll } = useLocomotiveScroll()

  useEffect(() => {
    if (scroll) {
      const element = scroll?.el

      scroll.on('scroll', () => {
        ScrollTrigger.update()
        ScrollTrigger.refresh()
      })
      ScrollTrigger.scrollerProxy(element, {
        scrollTop(value: number) {
          return arguments.length
            ? scroll.scrollTo(value, { duration: 0, disableLerp: true })
            : scroll.scroll.instance.scroll.y
        },
        getBoundingClientRect() {
          return {
            top: 0,
            left: 0,
            width: window.innerWidth,
            height: window.innerHeight,
          }
        },
        pinType: element.style.transform ? 'transform' : 'fixed',
      })
    }

    return () => {
      ScrollTrigger.addEventListener('refresh', () => scroll?.update())
      ScrollTrigger.refresh()
    }
  }, [scroll])

  return null
}
gsap.registerPlugin(ScrollTrigger);

function MyApp({ Component, pageProps }) {
  const { asPath } = useRouter()
  const containerRef = useRef(null) as MutableRefObject<HTMLDivElement | null>

  return (
    // <RLSProvider
    //   options={{
    //     smooth: true,
    //     // ... all available Locomotive Scroll instance options
    //   }}
    //   watch={
    //     [
    //       //..all the dependencies you want to watch to update the scroll.
    //       //  Basicaly, you would want to watch page/location changes
    //       //  For exemple, on Next.js you would want to watch properties like `router.asPath` (you may want to add more criterias if the instance should be update on locations with query parameters)
    //     ]
    //   }
    //   location={asPath}
    //   onLocationChange={(scroll: any) =>
    //     scroll.scrollTo(0, { duration: 0, disableLerp: true })
    //   }
    //   containerRef={containerRef}
    // >
    //   <ScrollTriggerProxy />

    <SmootherProvider>
      <MenuProvider>
        <Layouts containerRef={containerRef}>
          <Component {...pageProps} />
        </Layouts>
      </MenuProvider>
    </SmootherProvider>
    // </RLSProvider>
  )
}

export default MyApp
