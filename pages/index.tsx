import Head from 'next/head';
import About from '../components/About';
import Header from '../components/Header';
import Loader from '../components/Loader';
import Menu from '../components/Menu';
import { useContext, useRef, useState } from 'react';
import useIsomorphicLayoutEffect from '../components/use-isomorpphic-layout-effect';
import Projects from '../components/Projects';
import disableScroll from 'disable-scroll';
import Contact from '../components/Contact';
import Background from '../components/Background';
import { ScrollTrigger } from '../components/gsap'
import GScroll from "@grcmichael/gscroll"
import SmootherContext from 'context/SmootherContext';
import MenuContext from 'context/MenuContext';

export default function Home() {
  const { scrollRef } = useContext(SmootherContext);
  const scrollToSection = (id: string) => {
    scrollRef.current ? scrollRef.current.scrollTo(id) : null;
  };

  return (
    <>
      <Head>
        <title>Vo Hoang Duc Khoa</title>
        <meta name="title" content="Vo Hoang Duc Khoa's Portfolio" />
        <meta name='author' content='Vo Hoang Duc Khoa' />
        <meta name="description" content="My name is Khoa and I’m a frontend developer. I love learning new technologies and solving problems to create high-quality user experiences." />
        <meta name="keywords" content="developer, frontend, website, design, software, architecture, React.js, Javascript, HTML, CSS" />
        <meta name="robots" content="index, follow" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="English" />
        <meta name="revisit-after" content="30 days" />

        <meta property="og:title" content="Vo Hoang Duc Khoa's Portfolio" />
        <meta property="og:description" content="My name is Khoa and I’m a frontend developer. I love learning new technologies and solving problems to create high-quality user experiences." />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="https://www.duckhoa.dev/" />
        <meta property="og:image" content="/notion-avatar-khoa.svg" />

        <meta
          name='viewport'
          content='width=device-width, initial-scale=1.0, maximum-scale=5.0'
        />
        <meta name='theme-color' content='#171717' />
        <link
          rel='apple-touch-icon'
          sizes='180x180'
          href='/apple-touch-icon.png'
        />
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <div className='relative'>
        <Header
          scrollToSection={scrollToSection}
        />
        <Background />
      </div>
      <div className='bg-white'>
        <About />
        <Projects />
      </div>
      <Contact />
    </>
  );
}
