import Loader from '@/components/Loader';
import Menu from '@/components/Menu';
import { Ref, FC, useRef, useState, useContext } from 'react'
import GScroll from "@grcmichael/gscroll"
import { ScrollTrigger } from '@/components/gsap';
import useIsomorphicLayoutEffect from '@/components/use-isomorpphic-layout-effect';
import disableScroll from 'disable-scroll';
import MenuContext, { MenuProvider } from 'context/MenuContext';
import SmootherContext, { SmootherProvider } from 'context/SmootherContext';

type LayoutProps = {
  children?: React.ReactNode;
  containerRef?: Ref<HTMLDivElement>
}

const Layouts: FC<LayoutProps> = ({ children, containerRef }: LayoutProps) => {
  const { toggleSmootherReady, scrollRef, setScrollRef } = useContext(SmootherContext)

  const scrollToSection = (id: string) => {
    scrollRef.current ? scrollRef.current.scrollTo(id) : null;
  };

  useIsomorphicLayoutEffect(() => {
    setScrollRef(new GScroll("#smooth-wrapper", 0.6, ScrollTrigger.update))

    scrollRef.current.init()
    scrollRef.current.wheel()

    const scroller = document.querySelector("#smooth-wrapper")
    ScrollTrigger.defaults({
      scroller
    })

    ScrollTrigger.scrollerProxy(scroller, {
      scrollTop(value: number) {
        if (arguments.length) {
          scrollRef.current.current = -value
        }
        return -scrollRef.current.current
      },
      getBoundingClientRect() {
        return {
          top: 0,
          left: 0,
          width: window.innerWidth,
          height: window.innerHeight
        }
      }
    })
    disableScroll.on();

    window.addEventListener("resize", scrollRef.current.resize)

    scrollRef.current.resize()

    toggleSmootherReady(true);
  }, [])

  return (
    <main className='font-nunito overflow-x-hidden'>
      <div id='smooth-wrapper'>
        {children}
      </div>
      <Loader scrollToSection={scrollToSection} />
      <Menu
        scrollToSection={scrollToSection}
      />
    </main>
  )
}

export default Layouts