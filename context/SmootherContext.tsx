import React, { createContext, useRef } from "react";
import { useState } from "react";
import GScroll from "@grcmichael/gscroll"

interface SmootherContext {
  toggleSmootherReady?: (b: boolean) => void;
  smootherReady: boolean;
  setScrollRef?: (ref: React.MutableRefObject<GScroll>) => void
  scrollRef?: React.MutableRefObject<GScroll>
}
const SmootherContext = createContext<SmootherContext>({ smootherReady: false });

export const SmootherProvider = ({ children }) => {
  const [smootherReady, setSmootherReady] = useState(false);
  const scrollRef = useRef<GScroll>();

  const toggleSmootherReady = (value: boolean) => {
    setSmootherReady(value);
  };

  const setScrollRef = (ref: React.MutableRefObject<GScroll>) => (scrollRef.current = ref);

  return (
    <SmootherContext.Provider
      value={{
        toggleSmootherReady,
        smootherReady,
        setScrollRef,
        scrollRef
      }}
    >
      {children}
    </SmootherContext.Provider>
  );
};

export default SmootherContext;
