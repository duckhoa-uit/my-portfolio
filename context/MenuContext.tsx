import React, { useState } from "react";

interface MenuContext {
  menuOpen: boolean;
  toggleMenu?: React.Dispatch<React.SetStateAction<boolean>>;
}

const defaultState = {
  menuOpen: false,
};

const MenuContext = React.createContext<MenuContext>(defaultState);

export const MenuProvider = ({ children }) => {
  const [menuOpen, setMenuOpen] = useState(false);


  return (
    <MenuContext.Provider
      value={{
        menuOpen,
        toggleMenu: setMenuOpen,
      }}
    >
      {children}
    </MenuContext.Provider>
  );
};

export default MenuContext;
