/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: [{ loader: "@svgr/webpack", options: { icon: true } }],
    });

    return config;
  },
  typescript: {
    ignoreBuildErrors: true,
  },
  output: "standalone",
};

const withPlugins = require("next-compose-plugins");
const withTM = require("next-transpile-modules")([
  "gsap",
  "@grcmichael/gscroll",
]);
module.exports = withPlugins([withTM], nextConfig);
// module.exports = nextConfig;
